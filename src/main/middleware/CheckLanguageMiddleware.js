const langAccess = ['en', 'ru'];

module.exports = (req, res, next) =>
{
	let langGet = req.query.lang;
	let langCookie = req.language?.substr(0, 2);

	let lang = langAccess.find(lang => lang == langGet) || langAccess.find(lang => lang == langCookie) || 'en';

	if (langGet != lang)
	{
		let newSeach = new URLSearchParams(req._parsedUrl.search);
			newSeach.set('lang', lang);

		res.redirect(`?${newSeach.toString()}`);
	}
	else
	{
		next();
	}
}
