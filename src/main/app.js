require('dotenv').config();

const createError     = require('http-errors');
const express         = require('express');
const path            = require('path');
const requestLanguage = require('express-request-language');
const cookieParser    = require('cookie-parser');
const logger          = require('morgan');

const CheckLanguageMiddleware = require('./middleware/CheckLanguageMiddleware');

const showErrorStack = true;
const app = express();

// view engine setup
app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'ejs');

app.use(logger('short'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(requestLanguage(
{
	languages: ['en-US', 'ru-RU'],
	cookie:
	{
		name: 'language',
		options: { maxAge: 24*3600*1000 },
		url: '/languages/{language}'
	}
}));

app.use('/', express.static(path.join(__dirname, '..', '..', 'public', 'main')));

app.use('/users', [CheckLanguageMiddleware], require('./routes/usersRouter'));
app.use('/', [CheckLanguageMiddleware], require('./routes/indexRouter'));


// catch 404 and forward to error handler
app.use((req, res, next) =>
{
	next(createError(404));
});

// error handler
app.use((err, req, res, next) =>
{
	res.locals.errorStack = '';
	if (req.app.get('env') === 'development' && showErrorStack)
	{
		res.locals.errorStack = err.stack;
	}

	res.locals.errorStatus = err.status || 500;

	res.status(res.locals.errorStatus);
	res.render('error', {lang: req.query.lang || 'en'});
});


module.exports = app;
